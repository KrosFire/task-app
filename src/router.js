import Vue from 'vue';
import Router from 'vue-router';

import ProjectView from '@/content/ProjectView.vue';
import HomePage from '@/content/DefaultView.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/project/:id',
      name: 'Project',
      component: ProjectView,
    }, {
      path: '/',
      name: 'Home',
      component: HomePage,
      params: true,
    },
  ],
});
