import Vue from 'vue';
import Vuex from 'vuex';
import { db } from './firebase';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    showPopup: false,
    project: false,
    editP: false,
    editT: false,
    editId: undefined,
    delete: false,
    projects: [],
    dataReady: false,
    asideHidden: false,
    openToggler: false,
    mobile: false,
  },
  mutations: {
    // FIREBASE CONNECT
    bindFirebase(state, artData) {
      this.state.projects.push(artData);
      this.state.dataReady = true;
    },
    dataReady() {
      this.state.dataReady = true;
    },

    // PUSH TASK
    pushTask(state, { taskName, id }) {
      this.state.projects[id].tasks.push({
        complete: false,
        name: taskName,
      });
    },

    // CHECK TASK
    checkTask(state, { val, id, taskId }) {
      this.state.projects[id].tasks[taskId].complete = val;
    },

    // DELETE TASK
    deleteTask(state, { id, taskId }) {
      this.state.projects[id].tasks.splice(taskId, 1);
    },

    // EDIT TASK
    editTask(state, { val, id }) {
      const taskId = this.state.editId;
      this.state.projects[id].tasks[taskId].name = val;
    },

    // CREATE PROJECT
    submitProject(state, { title, date }) {
      this.state.projects.push({
        tasks: [],
        title,
        id: '',
        date,
      });
    },

    // DELETE PROJECT
    deleteProject(state, { id }) {
      this.state.projects.splice(id, 1);
    },

    // UPDATE ID
    updateId(state, { randomId }) {
      const pId = this.state.projects.length - 1;
      this.state.projects[pId].id = randomId;
    },

    // EDIT PROJECT
    editProject(state, { val, id }) {
      this.state.projects[id].title = val;
    },
    // Popups
    openPopup() {
      this.state.showPopup = true;
    },
    closePopup() {
      this.state.showPopup = false;
      this.state.project = false;
      this.state.editT = false;
      this.state.editP = false;
      this.state.delete = false;
    },

    // OPEN TOGGLER
    openToggler() {
      if (this.state.openToggler) {
        this.state.openToggler = false;
      } else {
        this.state.openToggler = true;
      }
    },

    // Navbar Aside
    showAside() {
      this.state.asideHidden = false;
    },
    closeAside() {
      this.state.asideHidden = true;
    },

    openProject() {
      this.state.project = true;
    },

    openEdit() {
      this.state.editP = true;
    },

    openEditT(state, { id }) {
      this.state.editT = true;
      this.state.editId = id;
    },

    openDelete() {
      this.state.delete = true;
    },

    // MOBILE VERSION
    mobileSet(state, { val }) {
      this.state.mobile = val
    }
  },
  actions: {
    // CONNCET FIREBASE
    connFirebase() {
      db.collection('Projects').orderBy('date').get().then((querySnapshot) => {
        querySnapshot.forEach((project) => {
          // Setting id
          const artData = project.data();
          artData.id = project.id;

          this.commit('bindFirebase', artData);
        });
        this.commit('dataReady');
      });
    },

    // UPDATE FIREBASE
    update(state, { id }) {
      const docId = this.state.projects[id].id;
      db.collection('Projects').doc(docId).set(this.state.projects[id]);
    },

    // ADD TASK FIREBASE
    addTask(state, { taskName, id }) {
      this.commit('pushTask', { taskName, id });

      this.dispatch('update', { id });
    },

    // CHECK TASK FIREBASE
    completeTask(state, { val, id, taskId }) {
      this.commit('checkTask', { val, id, taskId });
      this.dispatch('update', { id });
    },

    // DELETE TASK FIREBASE
    deleteTask(state, { id, taskId }) {
      this.commit('deleteTask', { id, taskId });
      this.dispatch('update', { id });
    },

    // CREATE PROJECT FIREBASE
    createProject(state, { title }) {
      const options = {
        year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric',
      };
      const date = new Date().toLocaleString('pl-PL', options);
      this.commit('submitProject', { title, date });
      this.commit('closePopup');
      const id = this.state.projects.length - 1;
      db.collection('Projects').add(this.state.projects[id]).then((p) => {
        const randomId = p.id;
        this.commit('updateId', { randomId });
      });
    },

    // DELETE PROJECT FIREBASE
    deleteProject(state, { id }) {
      const docId = this.state.projects[id].id;
      this.commit('deleteProject', { id });
      db.collection('Projects').doc(docId).delete();
    },
    openPopup(state, type) {
      this.commit('openPopup');
      if (type === 'project') {
        this.commit('openProject');
      } else if (type === 'edit') {
        this.commit('openEdit');
      } else if (type === 'delete') {
        this.commit('openDelete');
      }
    },
  },
});
