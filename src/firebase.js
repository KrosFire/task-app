import { initializeApp } from 'firebase';

const app = initializeApp({
  apiKey: 'AIzaSyBDvZ_g-M0qFqEJsdDwZg41-FwOnici3FQ',
  authDomain: 'task-app-1a3e7.firebaseapp.com',
  databaseURL: 'https://task-app-1a3e7.firebaseio.com',
  projectId: 'task-app-1a3e7',
  storageBucket: 'task-app-1a3e7.appspot.com',
  messagingSenderId: '869347807822',
  appId: '1:869347807822:web:7e1642037c9292b3',
});

// eslint-disable-next-line import/prefer-default-export
export const db = app.firestore();
